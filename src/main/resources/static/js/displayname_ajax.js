let typingTimer;
let doneTypingInterval = 500;
let myInput = document.getElementById('input');

myInput.addEventListener('keyup', () => {
    clearTimeout(typingTimer);
    if (myInput.value) {
        typingTimer = setTimeout(verifyUsername, doneTypingInterval);
    }
});

function verifyUsername(){
	var modelObj = {
		displayName: $("#input").val()
	};
	
	$.ajax({
		type: "GET",
		url: "/api/displayname/" + modelObj.displayName,  
		data: modelObj.playerName,
		success: function(data){
			console.log(data);
			switch(data) {
			  case '400':
				document.getElementById("display-name-not").innerHTML = 
					'<i class="fa fa-check" style="font-size:24px;color:#2de35e;margin:0 5px"></i>Username Available';
			    break;
			  case '401':
				document.getElementById("display-name-not").innerHTML = 
					'<i class="fa fa-times" style="font-size:24px;color:red;margin:0 5px;"></i>Displayname Taken';
				break;
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			document.getElementById("error").innerHTML = 'Error';
		}
	});
}
