let typingTimer;
let doneTypingInterval = 500;
let myInput = document.getElementById('input');

myInput.addEventListener('keyup', () => {
    clearTimeout(typingTimer);
    if (myInput.value) {
        typingTimer = setTimeout(verifyUsername, doneTypingInterval);
    }
});

function verifyUsername(){
	var modelObj = {
		playerName: $("#input").val()
	};
	
	$.ajax({
		type: "GET",
		url: "/api/getmcuser/" + modelObj.playerName,  
		data: modelObj.playerName,
		success: function(data){
			console.log(data);
			document.getElementById("error").style.color = "red";
			document.getElementById("form-submit").disabled = true;
			switch(data) {
			  case '400':
				document.getElementById("error").innerHTML = 'Player eligible';
				document.getElementById("error").style.color = "#2de35e";
				document.getElementById("form-submit").disabled = false;
			    break;
			  case '401':
				document.getElementById("error").innerHTML = 'Could not find player';
				break;
			  case '403':
				document.getElementById("error").innerHTML = 'Player is already linked to an account';
				break;
			  case '404':
				document.getElementById("error").innerHTML = 'Player is not online';
				break;
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			document.getElementById("error").innerHTML = 'Error';
		}
	});
}
