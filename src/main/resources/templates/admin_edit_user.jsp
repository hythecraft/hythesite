
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
	<head>
		<title>HytheCraft</title>
		<div th:replace="fragments/meta :: meta"></div>
	
	</head>
	<body>
		<div id="main">
         <div th:replace="fragments/header :: header"></div>
			<div class="grid-item">
				    <h2>Admin edit user</h2>
				    <p th:text="${error}" style="color:red"></p>
				    <form action="#" th:action="@{/user/settings/initial}" th:object="${member}" th:method="post">
				    	
				    	<h5>Profile ID: </h5>
				    	<input style="width:400px;border:none" type="text" th:field="*{uuid}" readonly/>
				    	<p>Your profile's global ID</p>
				    	
    					<h5>Display Name: </h5>
    					<input style="width:400px;" type="text" th:field="*{displayName}" />
    					<p>The name that will be displayed to all users</p>
    					
        				<h5>MC UUID: </h5>
        				<input style="width:400px;" type="text" th:field="*{playerUUID}" />
        				<p>Your Minecraft Player's UUID</p>
        				
        				<h5>Roles: </h5>
        				<input style="width:400px;" type="text" th:field="*{groups}" />
        				<p>Your user's roles</p>
        				
        				<p><input class="form-submit" type="submit" value="Save" /> <input class="form-reset" type="reset" value="Reset" /></p>
   		 			</form>
			</div>
		</div>
		
		<script src="https://mcapi.us/scripts/minecraft.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	</body>
</html>

