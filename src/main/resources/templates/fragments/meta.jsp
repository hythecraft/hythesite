<div th:fragment="meta">
	<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="RPG, MMORPG, Minecraft, MC, Minecraft RPG, Minecraft MMO, Minecraft MMORPG">
	
		<link rel="stylesheet" th:href="@{/css/main.css}" type="text/css"/>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="shortcut icon" th:href="@{/images/LogoMini.png}" />
</div>