<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
   <head>
      <meta charset="utf-8">
      <link rel="stylesheet" th:href="@{/css/main.css}" type="text/css"/>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   </head>
   <body>
      <div th:fragment="header">
      	 <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class=" w-100 order-1 order-md-0">
               <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                     <a class="nav-link" href="/home">Home</a>
                  </li>
                  <li class="nav-item active">
                     <a class="nav-link" href="/play">Play</a>
                  </li>
                  <li class="nav-item active">
                     <a class="nav-link" href="/about">About</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">Forum</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">Apply</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="#">Map</a>
                  </li>
               </ul>
            </div>
            <div class="mx-auto order-0">
               <a class="navbar-brand mx-auto" href="/home"><img style="width:10rem;margin:-10rem 0;" src="/images/HC_LOGO_GOLD.png"/></a>
            </div>
            <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item" th:if="${!logged_in}">
                  	 <a href="/oauth2/authorization/google" class="google-login-button">Login with <img class="google-image" src="../images/google.svg"/></a>
               	  </li>
             	  <li th:if="${logged_in}">
             	  	<a href="/user/settings" class="google-login-button">
             	  		 <span th:if="${logged_in_dat != null}">Logged in as <span th:text="${logged_in_dat}"></span></span>
             	  		 <span th:if="${logged_in_dat == null}">Logged in </span>
					</a>
             	  
             	  </li>
                  <li class="nav-item">
                     <script>
                        function copyToClipboard(element) {
                        	  var $temp = $("<input>");
                        	  $("body").append($temp);
                        	  $temp.val($(element).text()).select();
                        	  document.execCommand("copy");
                        	  $temp.remove();
                        	}
                        
                     </script>
                     <abbr title="Click to copy to Clipboard">
                        <div onclick="copyToClipboard('#cp');" class="ip-display" style="background-color: #fa8b1b;padding:5px;">
                           <span id="cp">
                              hythecraft.com
                              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clipboard" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                 <path fill-rule="evenodd" d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
                                 <path fill-rule="evenodd" d="M9.5 1h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
                              </svg>
                           </span>
                        </div>
                     </abbr>
                  </li>
                  <li class="nav-item">
                     <div onclick="window.location.href='https://discordapp.com/invite/F6gfvkv'" class="enlargen">
                        <span><img class ="enlargen-img" src="/images/discord.png"><span class="enlargen-text"> Discord</span></span>
                     </div>
                  </li>
               </ul>
            </div>
         </nav>
         <a href="/logout"><span th:if="${logged_in}" class="logout-button">Log out 
         <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-bar-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  			<path fill-rule="evenodd" d="M10.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 8l-2.647-2.646a.5.5 0 0 1 0-.708z"/>
  			<path fill-rule="evenodd" d="M6 8a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H6.5A.5.5 0 0 1 6 8zm-2.5 6a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 1 0v11a.5.5 0 0 1-.5.5z"/>
		</svg>
         </span></a>
         <br>
      </div>
   </body>
</html>