<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
   <head>
      <title>HytheCraft - Folding At Home</title>
      <div th:replace="fragments/meta :: meta"></div>
   </head>
   <body>
      <div id="main">
         <div th:replace="fragments/header :: header"></div>
         <div class="container" style="
         width:80%;background-color:white;border-radius:25px;padding:30px !important;
	 	 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            <div>
                <p>
               		In lieu of the Corona Virus crisis a lot of funding has been disrupted towards medical research. Here at Hythe we hold values to support our community when in times of need.
					We have started a F@H (Folding at Home) team to support the research of medicine in disease. 
					<b>Team# 266940</b>
			    </p>	
				<p>	
					The best thing is, anyone can participate for free. All it takes is some of your computer processing.
				</p>
				<p>
					Your help is much appreciated
					<br>
					-Thanks, the HytheCraft team
   				</p>
   				<a href="https://stats.foldingathome.org/team/266940">Team Page</a>
            </div>
         </div>
      </div>
  
   </body>
   
</html>