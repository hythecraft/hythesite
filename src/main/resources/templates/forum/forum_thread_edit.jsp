<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
   <head>
      <title>HytheCraft - About</title>
      <div th:replace="fragments/meta :: meta"></div>
   </head>
   <body>
      <div id="main">
         <div th:replace="fragments/header :: header"></div>
         <div class="container">
        
         </div>
      </div>
  
   </body>
   
</html>