<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
   <head>
      <title>HytheCraft - About</title>
      <link rel="stylesheet" th:href="@{/css/forum.css}" type="text/css"/>
      <div th:replace="fragments/meta :: meta"></div>
   </head>
   <body>
      <div id="main">
         <div th:replace="fragments/header :: header"></div>
         <div class="grid-item">
	        <h1>HytheCraft Forums</h1>
        	<div th:object="${frmMng}">
        		<hr>
	        		<div th:onclick="@{${'window.location.href=''/forum/' + cat.name + ''''}}" th:each="cat : *{categories}" class="category-box">
    	    				<h5 th:text="${cat.name}"></h5>
        					<p style="font-size:12px" th:text="${cat.description}"></p>
        					<hr>
        			</div>
         	</div>
         </div>
      </div>
  
   </body>
   
</html>