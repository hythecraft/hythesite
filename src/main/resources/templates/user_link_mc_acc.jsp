
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
	<head>
		<title>HytheCraft</title>
		<div th:replace="fragments/meta :: meta"></div>
	
	</head>
	<body>
		<div id="main">
			<div th:replace="fragments/header :: header">
			</div>
			<div class="grid-item">
				    <h2>Link your minecraft account</h2>
				    <span id="error" th:text="${error}" style="color:red"></span>
				    <form action="#" th:action="@{/user/settings/link}" th:object="${mcuser}" th:method="post">
				    	
				    	<h5>Minecraft username: </h5>
				    	<input style="width:400px;" type="text" th:field="*{playerName}" id="input"/>
				    	<p>The minecraft account you wish to link to</p>
				    		<br>
    					
    					<p>A prompt will pop up in game to ask you to link accounts</p>
    					<p>You must be online in-game to link accounts</p>
        				<p><input class="form-submit" id="form-submit" type="submit" value="Link" disabled/>        			
        			</form>
			
			</div>
		</div>
		
		<script src="https://mcapi.us/scripts/minecraft.js"></script>
		<script src="/js/mc_user_ajax.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	</body>
</html>

