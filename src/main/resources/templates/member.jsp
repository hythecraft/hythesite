<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
   <head>
      <title>HytheCraft - Member</title>
      
      <link rel="stylesheet" th:href="@{/css/member.css}" type="text/css"/>
      <div th:replace="fragments/meta :: meta"></div>
   </head>
   <body>
      <div id="main">
         <div th:replace="fragments/header :: header"></div>
         	<div class="center">
        	 	<div class="member-box">
        			<p th:utext="${profile}"></p>
      			</div>
      		</div>
      </div>
   </body>
</html>