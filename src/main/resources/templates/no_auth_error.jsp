<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
   <head>
      <title>HytheCraft - Error</title>
      <div th:replace="fragments/meta :: meta"></div>
   </head>
   <body>
      <div id="main">
         <div th:replace="fragments/header :: header"></div>
         <div class="center">
            <h2>Not enough permission.</h2>
            <h1><b>HYTHECRAFT</b></h1>
            <a href="/home"><button class="return-button">Return to home</button></a>
         </div>
      </div>
   </body>
</html>