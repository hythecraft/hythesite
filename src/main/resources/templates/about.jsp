<!DOCTYPE html>
<html  xmlns:th="http://www.thymeleaf.org">
   <head>
      <title>HytheCraft - About</title>
      <div th:replace="fragments/meta :: meta"></div>
   </head>
   <body>
      <div id="main">
         <div th:replace="fragments/header :: header"></div>
         <div class="container" style="
         width:80%;background-color:white;border-radius:25px;padding:30px !important;
	 	 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            <div>
               <h1>About Us</h1>
               <p> 
                  HytheCraft is an ambitious project which aims to take vanilla Minecraft and flip it on it's head.
                  HytheCraft is an MMORPG, in Minecraft. With custom items, mobs, quests, boss fights and a large open world, We aim to give the player a fun, immersive experience.
               </p>
               <br>
               <br>
               <p>
                  Why, How, When?
               </p>
               <br>
               <p>
                  HytheCraft started as a passion project and soon grew to be a large scale project. Our experienced team of developers have a great passion in building
                  and developing the world of Hythe to give our players the best, unique experience possible. 
               </p>
               <br>
               <p>
                  Here at Hythe we aim to push and break boundaries. Through countless hours of development we aim to provide features never seen or done in Minecraft before!
                  Our experienced team of Developers passionately work on HytheCraft in hopes of giving an awesome, awe inspiring game.
               </p>
               <br>
               <p>
                  At the moment, we don't have a set date to when we will launch. Hythe is a huge project, and thus it is very difficult to estimate a launch date.
                  When we do release an open-alpha we will select from a list of applicants who would like to play test the game.
                  To apply for our alpha program, register <a href="apply">Here</a>
               </p>
               <br>
               <br>
               <h2>Can't wait to see you in the world of Hythe!</h2>
               <h2>The HytheCraft Team</h2>
            </div>
         </div>
      </div>
  
   </body>
   
</html>