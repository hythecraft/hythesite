
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
	<head>
		<title>HytheCraft</title>
		<div th:replace="fragments/meta :: meta"></div>
	
	</head>
	<body>
		<div id="main">
			<div th:replace="fragments/header :: header">
			</div>
			<div class="container">
				<div class="grid-container">
					<div class="grid-item" style="grid-column-start:3;grid-row-start:1;grid-row-end:4;text-align:center;">
						<h5>Players online</h5>
						<h6><b>hythecraft.com</b></h6>
					</div>
					<div class="grid-item" style="
					padding: 0;
					overflow: hidden;
					background-image:url('../images/Banner1_copy.png');
					background-position: center;
					background-size: cover;
					grid-column-start:1;grid-column-end:3;grid-row-start: 1;">
					<h1 style="padding: 20px;color:white"><b>Want to play on HytheCraft?</b></h1>
					<p class ="server-now" style="padding: 0 20px;color:white">Join <b>x</b> other players on Hythecraft</p>
					
					</div>
					
					<div class="grid-item" style="grid-column-start:1;grid-column-end:3;grid-row-end:4;text-align:center;">
						<h5>Latest news</h5>
						<br>
						<span th:utext="${news}"></span>
					</div>		
				</div>
			
			</div>
			
		</div>
		
		
		<script src="https://mcapi.us/scripts/minecraft.js"></script>
		<script src="/js/server-status.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	</body>
</html>

