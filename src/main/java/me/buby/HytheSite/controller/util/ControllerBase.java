package me.buby.HytheSite.controller.util;

import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.services.user.Member;

public class ControllerBase {

	public static ModelAndView authMv(ModelAndView mv, Authentication auth)
	{
		mv.addObject("logged_in", (auth != null && auth.isAuthenticated()));
		if(auth != null && auth.isAuthenticated())
		{
			Member mem = null;
			if(HytheSiteApplication.mongo.activityExists(auth.getName(), "site-users"))
			{
				mem = Member.getMember(auth.getName());
			}
			else
			{
				mem = new Member(auth.getName());
			}
				
			mv.addObject("logged_in_dat", mem.displayName);
			if(mem.displayName == null)
			{
				ModelAndView v = new ModelAndView();
				v.setViewName("user_settings_initial");
				v.addObject("member", mem);
				return v;
			}
		}
		return mv;
	}
}




















