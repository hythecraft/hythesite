package me.buby.HytheSite.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.controller.util.ControllerBase;


@Controller
public class MainErrorController extends ControllerBase implements ErrorController  {
 
    @RequestMapping("/error")
    public ModelAndView handleError(Authentication auth, HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Integer statusCode = Integer.valueOf(status.toString());
        
        ModelAndView mv = new ModelAndView();
        mv.setViewName("error");			
        if(statusCode == HttpStatus.UNAUTHORIZED.value()) {
            mv.setViewName("no_auth_error");
        }
        
        mv = authMv(mv, auth);
        return mv;
    }
 
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
