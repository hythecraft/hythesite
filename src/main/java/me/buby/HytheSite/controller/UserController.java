package me.buby.HytheSite.controller;

import java.net.URL;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.controller.util.ControllerBase;
import me.buby.HytheSite.dao.MinecraftUserDao;
import me.buby.HytheSite.database.redis.util.RedisPacket;
import me.buby.HytheSite.database.redis.util.RedisPublish;
import me.buby.HytheSite.services.user.Member;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

@Controller
public class UserController extends ControllerBase{
	
	/*
	 * AJAX controllers
	 */
	
	/*
	 * End of AJAX Controllers
	 */
	
	@RequestMapping("/user/settings/link")
	public ModelAndView user_settings_link(Authentication auth, String error) {
		ModelAndView mv = new ModelAndView();
		MinecraftUserDao mcuser = new MinecraftUserDao();
		mv.addObject("error", error);
		mv.addObject("mcuser", mcuser);
		mv.setViewName("user_link_mc_acc");
		mv = authMv(mv, auth);
		
		return mv;
	}
	
	@PostMapping("/user/settings/link")
	public ModelAndView user_settings_link_post(@ModelAttribute MinecraftUserDao mcuser, Authentication auth) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("user_link_mc_acc");
		System.out.println("attempting to link " + mcuser.playerName);
		try {

		    Pattern PATTERN = Pattern.compile("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})");
			String url = "https://api.mojang.com/users/profiles/minecraft/"+mcuser.playerName;
			@SuppressWarnings("deprecation")
			String UUIDJson = IOUtils.toString(new URL(url));
			
			if(UUIDJson.isEmpty()) return user_settings_link(auth, "Could not find player " + mcuser.playerName);
			
			JSONObject UUIDObject = (JSONObject) JSONValue.parseWithException(UUIDJson);
			String uuid = PATTERN.matcher(UUIDObject.get("id").toString()).replaceFirst("$1-$2-$3-$4-$5");
			
			if(HytheSiteApplication.mongo.containsPlayerUUID(uuid, "site-users"))
				return user_settings_link(auth, mcuser.playerName + " is already linked to another account");
			
		    new RedisPublish(RedisPacket.SITE_TO_PLUGIN_REQUEST_LINK, mcuser.playerName, auth.getName());
		
		}catch(Exception e) {
			e.printStackTrace();
			return user_settings_link(auth, "Could not find player " + mcuser.playerName);
		}	
		mv = new ModelAndView();
		mv.setViewName("redirect:/user/settings");
		return mv;
	}
	
	@RequestMapping("/user/settings/initial")
	public ModelAndView user_settings_init(Authentication auth, String error){
		ModelAndView mv = new ModelAndView();
		if(Member.getMember(auth.getName()).displayName != null)
		{
			mv.setViewName("redirect:/user/settings");
			return mv;
		}
		mv.addObject("error", error);
		mv.addObject("member", Member.getMember(auth.getName()));
		mv.setViewName("user_settings_initial");
		return mv;
	}
	
	@PostMapping("/user/settings/initial")
	public ModelAndView user_settings_init_post(@ModelAttribute Member member, Authentication auth) {
		ModelAndView mv = new ModelAndView();
		if(member.displayName == null || member.displayName.isEmpty())
		{
			return user_settings_init(auth, "You need to set a display name");
		}
		if(HytheSiteApplication.mongo.containsName(member.displayName, "site-users"))
		{
			return user_settings_init(auth, "Displayname is taken");
		}
		HytheSiteApplication.mongo.saveData(member.uuid, member, "site-users");
		mv.setViewName("redirect:/home");
		return mv;
	}
	
	@GetMapping("/user/settings")
	public ModelAndView user_settings(Authentication auth) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("member", Member.getMember(auth.getName()));
		mv.setViewName("user_settings");
		mv = authMv(mv, auth);
		return mv;
	}
	
	@PostMapping("/user/settings")
	public ModelAndView user_settings_post(@ModelAttribute Member member, Authentication auth){

		HytheSiteApplication.mongo.saveData(member.uuid, member, "site-users");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("user_settings");
		mv = authMv(mv, auth);
		return mv;
	}

}




















