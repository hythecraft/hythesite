package me.buby.HytheSite.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.controller.util.ControllerBase;

@Controller
public class HomeController extends ControllerBase{

	@RequestMapping("home")
	public ModelAndView home(HttpServletRequest request, Authentication auth)
	{

		ModelAndView mv = new ModelAndView();
		HytheSiteApplication.init();

		if(HytheSiteApplication.news != null)
		mv.addObject("news", HytheSiteApplication.news.getNewsReel());

		mv = authMv(mv, auth);
		return mv;
	}
	
}




















