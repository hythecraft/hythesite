package me.buby.HytheSite.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.controller.util.ControllerBase;
import me.buby.HytheSite.services.user.Member;

@Controller
public class AdminController extends ControllerBase{
	
	@GetMapping(value="/admin/edit/{memberID}")
	public ModelAndView admin_edit_user(@PathVariable String memberID, Authentication auth) {
		if(!Member.getMember(auth.getName()).groups.contains("ADMIN"))
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("no_auth_error");
			mv = authMv(mv, auth);
			return mv;
		}
		
		ModelAndView mv = new ModelAndView();
		Member mem = null;
		mem = HytheSiteApplication.mongo.getObject(memberID, Member.class, "site-users");
		mv.addObject("member", mem);
		mv.setViewName("admin_edit_user");
        mv = authMv(mv, auth);
		return mv;
		
	}
	
	@PostMapping(value="/admin/edit/{memberID}")
	public ModelAndView admin_edit_user_post(@PathVariable Member member, Authentication auth){
		if(!Member.getMember(auth.getName()).groups.contains("ADMIN"))
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("no_auth_error");
			mv = authMv(mv, auth);
			return mv;
		}
		
		HytheSiteApplication.mongo.saveData(member.uuid, member, "site-users");
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv = authMv(mv, auth);
		return mv;
	}
	
}




















