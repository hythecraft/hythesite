package me.buby.HytheSite.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.controller.util.ControllerBase;

@Controller
public class AuthenticationController extends ControllerBase{

	@RequestMapping("logout")
	public ModelAndView logout(Authentication auth) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv = authMv(mv, auth);
		return mv;
	}

	@RequestMapping("login")
	public ModelAndView login(Authentication auth) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv = authMv(mv, auth);
		return mv;
	}

}




















