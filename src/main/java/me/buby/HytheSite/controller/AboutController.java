package me.buby.HytheSite.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.controller.util.ControllerBase;

@Controller
public class AboutController extends ControllerBase{
	@RequestMapping("about")
	public ModelAndView about(Authentication auth)
	{

		ModelAndView mv = new ModelAndView();
		mv.setViewName("about");
		mv = authMv(mv, auth);
		return mv;
	}

}




















