package me.buby.HytheSite.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.controller.util.ControllerBase;
import me.buby.HytheSite.services.user.Member;

@Controller
public class MemberController extends ControllerBase{

	@GetMapping(value="/member/{memberName}")
	public ModelAndView member(@PathVariable String memberName, Authentication auth) {
		ModelAndView mv = new ModelAndView();
		Member mem = null;
		if(HytheSiteApplication.mongo == null)
			return mv;
		
		mem = HytheSiteApplication.mongo.getObject(HytheSiteApplication.mongo.profileByName(memberName, "site-users"), Member.class, "site-users");
		
		mv.addObject("profile", mem.asHTML());
		mv.setViewName("member");

        mv = authMv(mv, auth);
		return mv;
	}
	
}




















