package me.buby.HytheSite.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import me.buby.HytheSite.controller.util.ControllerBase;

@Controller
public class MiscController extends ControllerBase{

	@RequestMapping("fah")
	public ModelAndView fah(Authentication auth) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("fah");
		mv = authMv(mv, auth);
		return mv;
	}

}




















