package me.buby.HytheSite.controller;

import java.net.URL;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.database.redis.util.RedisPacket;
import me.buby.HytheSite.database.redis.util.RedisPubResponse;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

/*
 * Used for API
 */

@RestController
public class ApiController {
	
	@GetMapping("/api/getmcuser/{dao}")
	public ResponseEntity<Object> postMCUser(@PathVariable String dao){
		try {

		    Pattern PATTERN = Pattern.compile("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})");
			String url = "https://api.mojang.com/users/profiles/minecraft/"+ dao;
			@SuppressWarnings("deprecation")
			String UUIDJson = IOUtils.toString(new URL(url));
			
			if(UUIDJson.isEmpty()) 
				return new ResponseEntity<>("401", HttpStatus.OK);
			
		
			JSONObject UUIDObject = (JSONObject) JSONValue.parseWithException(UUIDJson);
			String uuid = PATTERN.matcher(UUIDObject.get("id").toString()).replaceFirst("$1-$2-$3-$4-$5");
			
			if(HytheSiteApplication.mongo.containsPlayerUUID(uuid, "site-users"))
			{
				//Already linked
				return new ResponseEntity<>("403", HttpStatus.OK);
			}
			
			if(new RedisPubResponse().pulish(RedisPacket.SITE_TO_PLUGIN_PLAYER_ONLINE, RedisPacket.PLUGIN_TO_SITE_PLAYER_ONLINE, uuid, "n/a") == "404")
			{
				return new ResponseEntity<>("404", HttpStatus.OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			//Could not find player
			return new ResponseEntity<>("401", HttpStatus.OK);
		}	
		
		//Success
		return new ResponseEntity<>("400", HttpStatus.OK);
	}
	
	@GetMapping("/api/displayname/{displayname}")
	public ResponseEntity<Object> postDisplayName(@PathVariable String displayname){
		if(HytheSiteApplication.mongo.containsName(displayname, "site-users"))
			return new ResponseEntity<>("401", HttpStatus.OK);
		else
			return new ResponseEntity<>("400", HttpStatus.OK);
					
	}
}









