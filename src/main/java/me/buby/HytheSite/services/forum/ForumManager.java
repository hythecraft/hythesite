package me.buby.HytheSite.services.forum;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ForumManager {
	@Getter @Setter List<ForumCategory> categories = new ArrayList<>();
	
	public ForumManager() {
		categories.add(new ForumCategory("General", "General discussion"));
		categories.add(new ForumCategory("News", "Latest HytheCraft news"));
	}
}
