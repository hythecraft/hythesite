package me.buby.HytheSite.services.forum;

import lombok.Getter;
import lombok.Setter;

public class ForumCategory {
	@Getter @Setter public String name;
	@Getter @Setter public String description;
	
	public ForumCategory(String name ,String description) {
		this.name = name;
		this.description = description;
	}
}
