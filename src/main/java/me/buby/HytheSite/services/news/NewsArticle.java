package me.buby.HytheSite.services.news;

import me.buby.HytheSite.services.user.Member;

public class NewsArticle {
	public Member member;
	public String title;
	public String[] content;
	
	public NewsArticle(Member member, String title, String... content) {
		this.member = member;
		this.content = content;
		this.title = title;
	}
	
	public String getAsHTML() {
		String content = "";
		content += "<div class=\"news-item\">";
		content += "<div style=\"layout:grid\">";
		content += "<p>";
		content += "<img style=\"margin-right:10px\" class=\"news-author-image\" src=\"https://crafatar.com/avatars/"+member.playerUUID+"?overlay\"/>";
		content += "Authored by " + member.getPlayerReference();
		content += "</p>";
		content += "<hr>";
		content += "</div>";
		content += "<h5><b>" + title + "</h5></b>";
		for(String str : this.content)
		{
			content += str;
		}
		content += "</div>";
		return content;
	}
}
