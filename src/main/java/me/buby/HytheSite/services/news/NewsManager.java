package me.buby.HytheSite.services.news;

import java.util.ArrayList;
import java.util.List;

import me.buby.HytheSite.services.user.Member;

public class NewsManager {
	List<NewsArticle> articles = new ArrayList<>();

	public NewsManager() {
		articles.add(new NewsArticle(Member.getMember("105121664723092483226"), "Login & Linking to website",
				"<p>",
				"The HytheCraft website has undergone some major changes.",
				"We have been working hard to revampe the website with new features & a new look!",
				"</p>",
				
				"<p>",
				"You can now login with your Google account and link the account to your minecraft player!",
				" At the moment we don't have any features that utilise this, but in the future",
				" we will be adding many hooks into hythecraft to interact with the world outside of the game.",
				"</p>",
				
				"<p>",
				"Register today and get the opportunity to see new features and the latest news.",
				"</p>",
				
				"<p>",
				"Thanks, The Hythe Team.",
				"<br>",
				"https://hythecraft.com",
				"</p>"
				));
		
		articles.add(new NewsArticle(Member.getMember("105121664723092483226"), "Leaps and bounds!", 
				"<p>",
				"After our seemingly endless hiatus, we would like to proudly announce that HytheCraft is back on track. Due to many factors, we had to put Hythe on pause and focus on other endeavours. But alas, the time has come to start working on Hythe again. \r\n" + 
				"In the downtime the development team and I have tirelessly worked on re-write all of Hythe's code. We have fully revamped the website and added many new features that we are will be presenting in the weeks to come. We also have a huge map & cities that have been built which will be showcased very soon. \r\n" + 
				"</p>",
				
				"<p>",
				"Now, in regards to our alpha program- ",
				"We are at a point in development where we will soon be open to get players on and test the systems. Soon on our website we will be releasing a form you can fill out to sign up to the alpha program.",
				"At the moment, I can't give a definite date or time- but it will be announced.",
				"</p>",
				
				"<p>",
				"Can't wait to see you in Hythe! ",
				"</p>",
				
				"<p>",
				"Thanks, The Hythe Team.",
				"</p>",
				""));
	}
	
	public String getNewsReel() {
		String content = "";
		for(int i = 0; i < 5; i++) {
			if(articles.size() == i) break;
			if(articles.get(i) == null) continue;
			content += articles.get(i).getAsHTML();
		}
		return content;
	}
}
