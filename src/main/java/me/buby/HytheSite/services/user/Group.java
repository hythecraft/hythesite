package me.buby.HytheSite.services.user;

public enum Group {

	MEMBER("MEMBER", "", "#ffffff", 1),
	DEVELOPER("DEVELOPER", "Developer", "#f75d52", 10),
	STAFF("STAFF", "Staff", "#26e2f0", 9);
	
	public String ID;
	public String displayName;
	public String colorHEX;
	public int weight;
	
	private Group(String ID, String displayName, String colorHEX, int weight) {
		this.ID = ID;
		this.displayName = displayName;
		this.colorHEX = colorHEX;
		this.weight = weight;;
	}
	
	public static Group getGroup(String ID) {
		for(Group g : Group.values())
		{
			if(g.ID.equals(ID.toUpperCase()))
				return g;
		}
		return null;
	}
	
	public String getTag() {
		return "<span style=\"display:inline;background-color:"+this.colorHEX+";font-size:10px;padding:0 5px;border-radius:25px;\">"+this.displayName+"</span>";
	}
}
