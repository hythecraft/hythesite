package me.buby.HytheSite.services.user;

import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import me.buby.HytheSite.HytheSiteApplication;

@NoArgsConstructor
public class Member {

	@Getter @Setter public String uuid;
	@Getter @Setter public String displayName;
	@Getter @Setter public String playerUUID;
	
	@Getter @Setter public List<String> groups;
	
	public Member(String uuid) {
		this.uuid = uuid;
		this.playerUUID = "";
		this.groups = Arrays.asList(new String[] {Group.MEMBER.ID});
		HytheSiteApplication.mongo.saveData(uuid, this, "site-users");
	}
	
	private Group getHeaviestGroup() {
		Group gr = null;
		if(groups == null) return Group.MEMBER;
		for(String str : groups) {
			Group g = Group.getGroup(str);
			if(g == null) continue;
			if(gr == null) gr = g;
			if(gr != null && g.weight > gr.weight) gr = g;
		}
		return gr;
	} 	
	
	public static Member getMember(String ID) {
		return HytheSiteApplication.mongo.getObject(ID, Member.class, "site-users");
	}
	
	public String asHTML() {
		String content = "";
		
		content += "<h1>";
		content += "<img style=\"border-radius:15px;\" src=\"https://crafatar.com/avatars/"+this.playerUUID+"?overlay\"/>";
		content += this.displayName;
		for(String str : groups) {
			Group g = Group.getGroup(str);
			if(g == null) continue;
			content += g.getTag();
		}
		content += "</h1>";
		content += "<p>Description description</p>";
		
		return content;
	}
	
	public String getPlayerReference() {
		return "<a href=\"/member/"+this.displayName+"\" style=\"color:"+ getHeaviestGroup().colorHEX+"\">" + this.displayName +"</a> <span style=\"display:inline;background-color:"+getHeaviestGroup().colorHEX+";font-size:10px;padding:0 5px;border-radius:25px;\">"+getHeaviestGroup().displayName+"</span>";
	}
}
