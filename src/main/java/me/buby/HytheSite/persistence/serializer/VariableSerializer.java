package me.buby.HytheSite.persistence.serializer;

public abstract class VariableSerializer {
	public abstract String serialize(Object obj);
	
	public abstract Object deserialize(String str);
}
