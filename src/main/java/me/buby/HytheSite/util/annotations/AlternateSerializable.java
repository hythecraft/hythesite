package me.buby.HytheSite.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import me.buby.HytheSite.persistence.serializer.VariableSerializer;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AlternateSerializable {
	Class<? extends VariableSerializer> value();
}
