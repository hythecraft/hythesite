package me.buby.HytheSite;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Override
	public void configure(HttpSecurity http) throws Exception{
		http.antMatcher("/**").authorizeRequests()
			.antMatchers("/login").denyAll()
			.antMatchers("/*", "/api/**", "/css/**", "/fonts/**", "/audio/**", "/images/**", "/js/**", "/member/**").permitAll()
			.anyRequest().authenticated()
		.and()
		.logout()
		.logoutUrl("/logout")
		.logoutSuccessUrl("/home")
		.and()
		.oauth2Login();
	}
	
}
