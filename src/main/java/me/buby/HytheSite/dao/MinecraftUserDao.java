package me.buby.HytheSite.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class MinecraftUserDao {
	@Getter @Setter public String playerName = "";
}
