package me.buby.HytheSite.database;

import java.lang.reflect.Field;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import me.buby.HytheSite.persistence.serializer.VariableSerializer;
import me.buby.HytheSite.util.annotations.AlternateSerializable;
import me.buby.HytheSite.util.annotations.DoNotSerialize;

public class MongoHook {
	final String uri = "mongodb://192.168.250.100:27017/?ssl=false";
	final String publicUri = "mongodb://hythecraft.com:27017/?ssl=false";
	
	@SuppressWarnings("rawtypes")
	MongoCollection collection = null;
	MongoClient mongoClient;
	MongoDatabase mongoDatabase;
	
	public MongoHook() {		
		Thread th = new Thread() {
	        @Override
			public void run() {
				init();
			}
		};
		th.run();
	}
	
	private void init() {
		try {
			//TODO change to local on deploy
			mongoClient = new MongoClient(new ServerAddress("192.168.250.100", 27017));			
			mongoDatabase = mongoClient.getDatabase("hythecraft");
			collection = mongoDatabase.getCollection("site-users");		
			}catch(Exception e) {
				e.printStackTrace();
			}
	}
	
	
	@SuppressWarnings("unchecked")
	public void saveData(String id, Object data, String collectionName) {
		this.collection = mongoDatabase.getCollection(collectionName);
		Document found = (Document) collection.find(new Document("_id", id)).first();
		if(found != null) {
			Field[] allFields = data.getClass().getDeclaredFields(); 
	    	for(Field field : allFields) {
	    		field.setAccessible(true);
	    		try {

	    			Bson updatedValue = new Document(field.getName(), field.get(data));
	        		if(field.getAnnotation(DoNotSerialize.class) != null) continue;
	    			if(field.getAnnotation(AlternateSerializable.class) != null) 
	    				updatedValue = new Document(field.getName(), ((VariableSerializer)field.getAnnotation(AlternateSerializable.class).value().getDeclaredConstructor().newInstance()).serialize(field.get(data)));
	    			
	    			Bson updateOperation = new Document("$set", updatedValue);
	    			collection.updateOne(found, updateOperation);
	    			
	    		}catch(Exception e) {
	    		}
	    	}
		}
		else {
			Document document = new Document("_id", id);
			Field[] allFields = data.getClass().getDeclaredFields(); 
	    	for(Field field : allFields) {
	    		field.setAccessible(true);
	    		try {
	    			if(field.getAnnotation(DoNotSerialize.class) != null) continue;
	    			if(field.getAnnotation(AlternateSerializable.class) != null) {
	    				document.append(field.getName(), ((VariableSerializer)field.getAnnotation(AlternateSerializable.class).value().getDeclaredConstructor().newInstance()).serialize(field.get(data)) );
	    				collection.insertOne(document);
	    				continue;
	    			}
	    			
	    			document.append(field.getName(), field.get(data));
	    			
	    		}catch(Exception e) {
	    		}
	    	}
	    	try {
			collection.insertOne(document);
	    	}catch(Exception e) {};
		}
		
		
		
	}
	
	public <T> T getObject(String id, Class<T> clazz, String collectionName) {
		this.collection = mongoDatabase.getCollection(collectionName);
		T obj = null;
		try {
		obj = clazz.getDeclaredConstructor().newInstance();
		}catch(Exception e) {}
		Field[] allFields = clazz.getDeclaredFields(); 
		Document document = (Document) collection.find(new Document("_id", id)).first();
		for(Field field : allFields) {
    		field.setAccessible(true);

    		Object val = null;
    		try {
    		if(field.getAnnotation(DoNotSerialize.class) != null) continue;
			if(field.getAnnotation(AlternateSerializable.class) != null) {
				val = ((VariableSerializer)field.getAnnotation(AlternateSerializable.class).value().getDeclaredConstructor().newInstance()).deserialize(document.getString(field.getName()));
			}
			else
				 val = document.get(field.getName());
			
    		}catch(Exception e) {}
    		
    		try {
    			field.set(obj, val);

    		}catch(Exception e) {}
		}
		return obj;

	}
	
	public boolean activityExists(String id, String collectionName) {
	    FindIterable<Document> iterable = mongoDatabase.getCollection(collectionName).find(new Document("_id", id));
	    return iterable.first() != null;
	}

	public boolean containsName(String displayName, String collectionName) {
	    FindIterable<Document> iterable = mongoDatabase.getCollection(collectionName).find(new Document("displayName", displayName));
	    return iterable.first() != null;
	}
	
	public boolean containsPlayerUUID(String playerUUID, String collectionName) {
	    FindIterable<Document> iterable = mongoDatabase.getCollection(collectionName).find(new Document("playerUUID", playerUUID));
	    return iterable.first() != null;
	}
	
	public String profileByName(String displayName, String collectionName) {
	    FindIterable<Document> iterable = mongoDatabase.getCollection(collectionName).find(new Document("displayName", displayName));
	    return iterable.first().getString("uuid");
	}
	
	public void disable() {
		mongoClient.close();
	}
}
