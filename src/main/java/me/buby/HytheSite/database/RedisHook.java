package me.buby.HytheSite.database;

import java.util.Map;

import lombok.Getter;
import me.buby.HytheSite.HytheSiteApplication;
import me.buby.HytheSite.database.redis.util.RedisPacket;
import me.buby.HytheSite.services.user.Member;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisHook {
	@Getter private Jedis jedis;
	
	public RedisHook() {
		jedis = new Jedis(RedisPacket.JEDIS_IP, RedisPacket.JEDIS_PORT);	
		
		Thread thread = new Thread() {
			public void run() {
				reciever();
			}
		};
		thread.start();
	}
	
	public void reciever() {
	    JedisPubSub jedisPubSub = new JedisPubSub() {
	    	
	        @Override
	        public void onPMessage(String pattern, String channel, String message) {
	        	 RedisPacket packet = RedisPacket.asPacket(message);
		            if(packet == null) return;
		            
		            Map<String, String> args = packet.getArgsIncoming(message);
		            
		            switch(packet) {
					case PLUGIN_TO_SITE_PLAYER_ONLINE:
						break;
					case PLUGIN_TO_SITE_ACCEPT_LINK:
						Member mem = Member.getMember(args.get("googleID"));
						mem.setPlayerUUID(args.get("uuid"));
						HytheSiteApplication.mongo.saveData(mem.uuid, mem, "site-users");
						break;
					default:
						return;
		            	
		            }
		            
		        }
	        
	        
	        
	    };
	    
	    jedis.psubscribe(jedisPubSub, RedisPacket.PRIMARY_CHANNEL);
	}
}
