package me.buby.HytheSite.database.redis.util;

import lombok.Getter;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisPublish extends JedisPubSub{
	
	@Getter private Jedis jedis;
	public RedisPublish(RedisPacket packet, String... args) {
		jedis = new Jedis(RedisPacket.JEDIS_IP, RedisPacket.JEDIS_PORT);	
		publish(packet, args);
	}
	
	public boolean publish(RedisPacket packet, String... args) {
		try {
			jedis.publish(RedisPacket.PRIMARY_CHANNEL, packet.getPacket(args));
			jedis.close();
			return true;
		}catch(Exception e) {
			jedis.close();
			return false;
		}
	}
	
}
