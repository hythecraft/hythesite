package me.buby.HytheSite.database.redis.util;

import java.util.HashMap;
import java.util.Map;

public enum RedisPacket {
	WORKER_TO_MANAGER_REGISTER("WORKER>MANAGER:REGISTER", "uuid"), //Called when a worker starts up
	WORKER_TO_MANAGER_RECIEVE_WEIGHT("WORKER>MANAGER:RECIEVE_WEIGHT", "uuid", "weight"), //Displays the weight of the worker
	WORKER_TO_MANAGER_PROVISION_ACKNOWLEDGE("WORKER>MANAGER:PROVISION_AKNW"), //Displays that the worker acknowledged the provision request
	WORKER_TO_MANAGER_PING("WORKER>MANAGER:PING", "uuid"),
	
	MANAGER_TO_WORKER_GET_WEIGHT("MANAGER>WORKER:GET_WEIGHT", "uuid", "weight"),
	MANAGER_TO_WORKER_PROVISION_WORKER("MANAGER>WORKER:PROVISION_WORKER", "uuid", "type", "port"),
	MANAGER_TO_WORKER_PING("MANAGER>WORKER:PING"),
	
	MANAGER_TO_OUTSIDE_LIST_WORKERS("MANAGER>OUTSIDE:LIST_WORKERS", "uuid", "list"),
	
	OUTSIDE_TO_MANAGER_LIST_WORKERS("OUTSIDE>MANAGER:LIST_WORKERS", "uuid"),
	OUTSIDE_TO_MANAGER_PROVISION_WORKER("OUTSIDE>MANAGER:PROVISION_WORKER", "uuid", "type"),
	
	SITE_TO_PLUGIN_PLAYER_ONLINE("SITE>PLUGIN:PLAYER_ONLINE", "uuid", "googleID"),
	PLUGIN_TO_SITE_PLAYER_ONLINE("PLUGIN>SITE:PLAYER_ONLINE", "uuid", "googleID"),
	
	SITE_TO_PLUGIN_REQUEST_LINK("SITE>PLUGIN:REQUEST_LINK", "uuid", "googleID"),
	PLUGIN_TO_SITE_ACCEPT_LINK("PLUGIN>SITE:ACCEPT_LINK", "uuid", "googleID");
	
	public final static String PRIMARY_CHANNEL = "abAlpha*";
	
	public final static String JEDIS_IP = "192.168.250.100";
	public final static int JEDIS_PORT = 6379;

	public String packetHeader;
	public String[] argumentNames;
	
	private RedisPacket(String packetHeader, String... argumentNames)
	{
		this.packetHeader = packetHeader;
		this.argumentNames = argumentNames;
	}
	
	public Map<String, String> getArgsIncoming(String message){
		Map<String, String> values = new HashMap<>();
		RedisPacket packetType = asPacket(message);
		
		String[] args = message.split("#");
		for(int i = 0; i < packetType.argumentNames.length; i++)
		{
			values.put(packetType.argumentNames[i], args[i+1]);
		}
		return values;
	}
	
	public String getPacket(String... args) throws RedisInsufficientArgsException
	{
		if(args.length != argumentNames.length)
		{
			throw new RedisInsufficientArgsException();
		}
		
		String packet = this.packetHeader;
		for(String arg : args)
			packet += "#" + arg;
		return packet;
	}

	public static RedisPacket asPacket(String msg) {
		for(RedisPacket packet : RedisPacket.values())
		{
			if(msg.contains(packet.packetHeader))
				return packet;
		}
		return null;
	}
}
 















