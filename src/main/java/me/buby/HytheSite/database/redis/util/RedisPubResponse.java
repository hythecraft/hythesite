package me.buby.HytheSite.database.redis.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisPubResponse {
	
	String response;
	int timeoutMillis = 500;
	public String pulish(RedisPacket packet, RedisPacket listenPacket, String... args) {
		try {
			Jedis jedis = new Jedis(RedisPacket.JEDIS_IP, RedisPacket.JEDIS_PORT);	
			jedis.publish(RedisPacket.PRIMARY_CHANNEL, packet.getPacket(args));
			jedis.close();
			Thread thread = new Thread() {
				public void run() {
					response(listenPacket);
				}
			};
			thread.start();
			Thread.sleep(timeoutMillis);
			return response == null ? "404" : response;
		}catch(Exception e) {}
		return "404";
	}
	
	private String response(RedisPacket listenPacket) {
		Jedis jedis = new Jedis(RedisPacket.JEDIS_IP, RedisPacket.JEDIS_PORT);	
		JedisPubSub jedisPubSub = new JedisPubSub() {
	    	
	        @Override
	        public void onPMessage(String pattern, String channel, String message) {
	        	 RedisPacket packet = RedisPacket.asPacket(message);
		            if(packet == null) return;
		            
		            if(packet.equals(listenPacket)) response = message;
		            jedis.close();
		        }
	        
	        
	        
	    };
	    
	    jedis.psubscribe(jedisPubSub, RedisPacket.PRIMARY_CHANNEL);
	    return response;
	}
}
