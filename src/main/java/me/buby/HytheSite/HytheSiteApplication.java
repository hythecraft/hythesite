package me.buby.HytheSite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;

import me.buby.HytheSite.database.MongoHook;
import me.buby.HytheSite.database.RedisHook;
import me.buby.HytheSite.services.forum.ForumManager;
import me.buby.HytheSite.services.news.NewsManager;

/* SecurityAutoConfiguration.class*/

@PropertySource({"classpath:application.properties"})
@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class, MongoAutoConfiguration.class})
public class HytheSiteApplication extends SpringBootServletInitializer{
	   
	public static NewsManager news = null;
	public static ForumManager forum = null;
	public static MongoHook mongo = null;
	public static RedisHook redis = null;
	
	public static void main(String[] args) {
		init();
	    System.setProperty("spring.devtools.restart.enabled", "true");
	    System.setProperty("tomcat.util.http.parser.HttpParser.requestTargetAllow", "{}");

	    SpringApplication.run(HytheSiteApplication.class, args);
	}
	

	public static void init() {
		redis = new RedisHook();
		mongo = new MongoHook();
		news = new NewsManager();
		forum = new ForumManager();
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(HytheSiteApplication.class);
	}
	

}

